#ifndef FILTER_H
#define FILTER_H

#include <QString>
#include <QDate>

enum commonOp_e{coNone, coGreater, coLess, coEqual};
enum datePeriod_e{dpNone, dpDay, dpWeek, dpMonth, dpYear};
enum textOp_e{toNone, toContains, toNotContains};


class Filter
{
public:
    Filter();
    QString toString();
    
    QString cat() const;
    void setCat(const QString &cat);
    
    QString scat() const;
    void setScat(const QString &scat);
    
    QString commentPattern() const;
    void setCommentPattern(const QString &commentPattern);
    
    commonOp_e sumOp() const;
    void setSumOp(const commonOp_e &sumOp);
    
    double sum() const;
    void setSum(double sum);
        
    commonOp_e dateOp() const;
    void setDateOp(const commonOp_e &dateOp);
    
    datePeriod_e datePeriod() const;
    void setDatePeriod(const datePeriod_e &datePeriod);
    
    int getDatePreiodCount() const;
    void setDatePreiodCount(int value);
    
    QDate getDate() const;
    void setDate(const QDate &value);
    
    void clear();
    
    textOp_e commentOp() const;
    void setCommentOp(const textOp_e& value);
    
private:
    QString cat_;
    QString scat_;
    textOp_e commentOp_;
    QString commentPattern_;        
    commonOp_e sumOp_;
    double sum_;
    commonOp_e dateOp_;
    QDate date_;
    datePeriod_e datePeriod_;
    int datePeriodCount_;
};

#endif // FILTER_H
