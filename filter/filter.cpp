#include "filter.h"

Filter::Filter() :
    cat_(""),
    scat_(""),
    commentOp_(toNone),
    commentPattern_(""),
    sumOp_(coNone),
    sum_(0.0),
    dateOp_(coNone),
    date_(QDate::currentDate()),
    datePeriod_(dpNone),
    datePeriodCount_(0)
{
}

QString Filter::toString()
{
    QString rez = QString("[filter]\n");
    rez += QString("cat:\t%1\n").arg(cat_);
    rez += QString("scat:\t%1\n").arg(scat_);
    rez += QString("commOp:\t%1\n").arg(commentOp_);
    rez += QString("comm:\t%1\n").arg(commentPattern_);
    rez += QString("sumOp:\t%1\n").arg(sumOp_);
    rez += QString("sum:\t%1\n").arg(sum_);
    rez += QString("dateOp:\t%1\n").arg(dateOp_);
    rez += QString("datePeriod:\t%1\n").arg(datePeriod_);
    rez += QString("datePeriodCount_:\t%1\n").arg(datePeriodCount_);
    return rez;
}

QString Filter::cat() const
{
    return cat_;
}

void Filter::setCat(const QString &cat)
{
    cat_ = cat;
}
QString Filter::scat() const
{
    return scat_;
}

void Filter::setScat(const QString &scat)
{
    scat_ = scat;
}
QString Filter::commentPattern() const
{
    return commentPattern_;
}

void Filter::setCommentPattern(const QString &commentPattern)
{
    commentPattern_ = commentPattern;
}

commonOp_e Filter::sumOp() const
{
    return sumOp_;
}

void Filter::setSumOp(const commonOp_e &sumOp)
{
    sumOp_ = sumOp;
}

double Filter::sum() const
{
    return sum_;
}

void Filter::setSum(double sum)
{
    sum_ = sum;
}

commonOp_e Filter::dateOp() const
{
    return dateOp_;
}

void Filter::setDateOp(const commonOp_e &dateOp)
{
    dateOp_ = dateOp;
}

datePeriod_e Filter::datePeriod() const
{
    return datePeriod_;
}

void Filter::setDatePeriod(const datePeriod_e &datePeriod)
{
    datePeriod_ = datePeriod;
}

int Filter::getDatePreiodCount() const
{
    return datePeriodCount_;
}

void Filter::setDatePreiodCount(int value)
{
    datePeriodCount_ = value;
}
QDate Filter::getDate() const
{
    return date_;
}

void Filter::setDate(const QDate &value)
{
    date_ = value;
}

void Filter::clear()
{
    cat_ = "";
    scat_ = "";
    commentOp_ = toNone;
    commentPattern_ = "";
    sumOp_ = coNone;
    sum_ = 0.0;    
    dateOp_ = coNone;
    date_ = QDate::currentDate();
    datePeriod_ = dpNone;
    datePeriodCount_ = 0;
}

textOp_e Filter::commentOp() const
{
    return commentOp_;
}

void Filter::setCommentOp(const textOp_e &value)
{
    commentOp_ = value;
}
