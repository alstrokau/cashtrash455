#ifndef FILTRATOR_H
#define FILTRATOR_H

#include "filter/filter.h"
#include "ds/database.h"

class Filtrator
{
public:
    Filtrator();
    static Database filtrate(const Filter& filter, const Database& database);    
private:
    
};

#endif // FILTRATOR_H
