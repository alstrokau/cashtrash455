#include "filtrator.h"

Filtrator::Filtrator()
{
}

Database Filtrator::filtrate(const Filter &filter, const Database &database)
{
    Transaction currTrans;
    Database rezDatabase;
    
    for (int i = 0; i < database.size(); ++i) {
        bool transPass = true;
        currTrans = database.item(i);
        
        if (!filter.cat().isEmpty() &&
                filter.cat() != currTrans.cat()) {
            transPass = false;
        }
        
        if(!filter.scat().isEmpty() &&
                  filter.scat() != currTrans.scat()){
            transPass = false;
        }
        
        if(!filter.commentPattern().isEmpty() && filter.commentOp() != toNone){
            if(filter.commentOp() == toContains){
                if(!currTrans.comment().contains(filter.commentPattern(), Qt::CaseInsensitive)){
                    transPass = false;
                }
            }else{
                if(currTrans.comment().contains(filter.commentPattern(), Qt::CaseInsensitive)){
                    transPass = false;
                }
            }
        }
        
        if(filter.sumOp() != coNone){
            switch (filter.sumOp()) {
            case coGreater:
                if(currTrans.sum() < filter.sum()){
                    transPass = false;
                }
                break;
            case coLess:
                if(currTrans.sum() > filter.sum()){
                    transPass = false;
                }
                break;
            case coEqual:
                if(currTrans.sum() != filter.sum()){
                    transPass = false;
                }
                break;
            default:
                break;
            }
        }
        
        switch (filter.dateOp()) {
        case coGreater:
            if(currTrans.datetime() < filter.getDate()){
                transPass = false;
            }
            break;
        case coLess:
            if(currTrans.datetime() > filter.getDate()){
                transPass = false;
            }
            break;
        case coEqual:
            if(currTrans.datetime() != filter.getDate()){
                transPass = false;
            }
            break;
        default:
            break;
        }

        if(filter.datePeriod() != dpNone){
            
            int factor = 1;
            
            switch (filter.datePeriod()) {
            case dpDay:
                factor = 1;
                break;
            case dpWeek:
                factor = 7;
                break;
            case dpMonth:
                factor = 30;
                break;
            case dpYear:
                factor = 365;
                break;
            default:
                break;
            }
            
            if(currTrans.datetime() < 
                    QDate::currentDate().addDays( (-factor)*filter.getDatePreiodCount())){
                transPass = false;
            }
        }

        
        if(transPass){
            rezDatabase.addTransaction(currTrans);
        }
    }
    
    rezDatabase.setInitBalance(database.getInitBalance());
    return rezDatabase;
}
