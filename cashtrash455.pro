#-------------------------------------------------
#
# Project created by QtCreator 2013-08-14T09:31:43
#
#-------------------------------------------------

QT       += core gui printsupport xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cashtrash455
TEMPLATE = app


SOURCES += main.cpp\
        win/cashtrash455mainwin.cpp \
        ds/transaction.cpp \
        ds/database.cpp \
        rr/abstractrenderer.cpp \
        rr/textrenderer.cpp \
        io/iodatabasestroage.cpp \
        io/ioxml.cpp \
        st/statistic.cpp \
        st/statisticreport.cpp \
    tl/pteLogger/ptelogger.cpp \
    win/dlgedittransaction.cpp \
    st/statavpm.cpp \
    st/abstractstatmodule.cpp \
    st/stattoppmtrans.cpp \
    st/stattoppmcats.cpp \
    filter/filter.cpp \
    filter/filtrator.cpp \
    qcustomplot/qcustomplot.cpp \
    rr/tablerenderer.cpp \
    rr/stattablerenderer.cpp \
    st/statcatscat.cpp \
    rr/chartsrenderer.cpp \
    ds/common.cpp \
    tl/SettingsProcessor/settingsprocessor.cpp

HEADERS  += win/cashtrash455mainwin.h \
        ds/transaction.h \
        ds/database.h \
        rr/abstractrenderer.h \
        rr/textrenderer.h \
        io/iodatabasestroage.h \
        io/ioxml.h \
        st/statistic.h \
        st/statisticreport.h \
    tl/pteLogger/ptelogger.h \
    win/dlgedittransaction.h \
    st/statavpm.h \
    st/abstractstatmodule.h \
    st/stattoppmtrans.h \
    st/stattoppmcats.h \
    filter/filter.h \
    filter/filtrator.h \
    qcustomplot/qcustomplot.h \
    rr/tablerenderer.h \
    rr/stattablerenderer.h \
    st/statcatscat.h \
    rr/chartsrenderer.h \
    ds/common.h \
    tl/SettingsProcessor/settingsprocessor.h

FORMS    += win/cashtrash455mainwin.ui \
    win/dlgedittransaction.ui

RESOURCES += \
    cashtrash455.qrc

INCLUDEPATH += \
    qcustomplot
