#include "ioxml.h"

const QString ioXml::dateFormat = "dd.MM.yyyy";

ioXml::ioXml()
{
}

void ioXml::writeToXml(const QString fname, const Database database)
{
    QFile f(fname);

    if (!f.open(QIODevice::WriteOnly)) {
        qDebug() << QString("cannot open [%1] to write base").arg(fname);
        return;
    }

    QTextStream tsXml(&f);
    tsXml.setCodec("UTF-8");
    tsXml.setGenerateByteOrderMark(true);

    tsXml << genXmlString(database);

    qDebug() << QString("write xml to [%1] norm").arg(fname);

    f.close();
}

Database ioXml::readFromXml(const QString fname)
{
    QFile f(fname);

    if (!f.exists()) {
        qDebug() << QString("file [%1] does not exist").arg(fname);
        return Database();
    }

    if (!f.open(QIODevice::ReadOnly)) {
        qDebug() << QString("cannot open [%1] to read base").arg(fname);
        return Database();
    }

    Database rezBase;
    QDomDocument doc;

    if (!doc.setContent(&f)) {
        qDebug() << QString("cannot read content of [%1]").arg(fname);
        return Database();
    }

    qDebug() << "--==reader==--";

    QDomElement deMain = doc.documentElement();
    QString sBuff = deMain.attribute("ib");
    rezBase.setInitBalance(sBuff.toDouble());
    qDebug() << "ib\t" << sBuff;

    QDomNode dnTransactions = doc.firstChild().firstChild();

    QDomNode dnTransaction = dnTransactions.firstChild();
    Transaction t;
    while (!dnTransaction.isNull()) {
        if (dnTransaction.isElement()) {
            QDomElement deTransaction = dnTransaction.toElement();
            t.setData(deTransaction.attribute("cat"),
                      deTransaction.attribute("sum").toDouble(),
                      deTransaction.attribute("scat"),
                      deTransaction.attribute("comm"),
                      QDate::fromString(deTransaction.attribute("date"), dateFormat));

            rezBase.addTransaction(t);
        }
        dnTransaction = dnTransaction.nextSibling();
    }

    f.close();
    qDebug() << "bsz\t" << rezBase.size();
    return rezBase;
}

QString ioXml::genXmlString(const Database database)
{
    QDomDocument doc;
    QDomElement deDatabase = doc.createElement("Database");
    deDatabase.setAttribute("ib", database.getInitBalance());

    QDomElement deTransContainer = doc.createElement("Transactions");

    for (int c = 0; c < database.size(); c++) {
        QDomElement deTrans = doc.createElement("Trans");
        deTrans.setAttribute("cat",     database.item(c).cat());
        deTrans.setAttribute("scat",    database.item(c).scat());
        deTrans.setAttribute("sum",     database.item(c).sum());
        deTrans.setAttribute("date",    database.item(c).datetime().toString(dateFormat));
        deTrans.setAttribute("comm",    database.item(c).comment());

        deTransContainer.appendChild(deTrans);
    }

    doc.appendChild(deDatabase);
    deDatabase.appendChild(deTransContainer);

    return doc.toString(4);
}
