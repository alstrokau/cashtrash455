#ifndef IODATABASESTROAGE_H
#define IODATABASESTROAGE_H

#include "ds/database.h"

class ioDatabaseStroage
{
public:
    ioDatabaseStroage();
    void setDatabase(const Database database);
    void clearDatabase();
    bool isEmpty() const;

private:
    Database db_;
    bool isEmpty_;
};

#endif // IODATABASESTROAGE_H
