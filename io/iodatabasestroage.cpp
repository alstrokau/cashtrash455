#include "iodatabasestroage.h"

ioDatabaseStroage::ioDatabaseStroage():
    isEmpty_(true){
}

void ioDatabaseStroage::setDatabase(const Database database){
    db_ = database;
    isEmpty_ = false;
}

void ioDatabaseStroage::clearDatabase(){
    db_.clear();
    isEmpty_ = true;
}

bool ioDatabaseStroage::isEmpty() const{
    return isEmpty_;
}
