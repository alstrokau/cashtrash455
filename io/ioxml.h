#ifndef IOXML_H
#define IOXML_H

#include <QtXml/qdom.h>
#include <QFile>

#include "iodatabasestroage.h"

class ioXml
{
public:
    ioXml();
    static void writeToXml(const QString fname, const Database database);
    static Database readFromXml(const QString fname);

    ioDatabaseStroage storage;

private:
    static QString genXmlString(const Database database);
    static const QString dateFormat;
};

#endif // IOXML_H
