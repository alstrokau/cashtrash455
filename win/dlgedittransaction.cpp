#include "dlgedittransaction.h"
#include "ui_dlgedittransaction.h"

dlgEditTransaction::dlgEditTransaction(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dlgEditTransaction)
{
    ui->setupUi(this);
    ui->deDate->setDate(QDate::currentDate());
    connect(this, SIGNAL(accepted()), this, SLOT(getDataToTransaction())); 
}

dlgEditTransaction::~dlgEditTransaction()
{
    delete ui;
}

Transaction &dlgEditTransaction::getTransaction()
{
    return tr_;
}

void dlgEditTransaction::getDataToTransaction()
{
    tr_.setData(ui->cbCat->currentText(), 
                ui->dsbSum->value(),
                ui->cbScat->currentText(), 
                ui->leComm->text(), 
                ui->deDate->date());
}
