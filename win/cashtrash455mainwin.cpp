#include "cashtrash455mainwin.h"
#include "dlgedittransaction.h"
#include "ui_cashtrash455mainwin.h"
#include <QMessageBox>


CashTrash455MainWin::CashTrash455MainWin(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CashTrash455MainWin) {
    ui->setupUi(this);
    
    apLogger.reset(new pteLogger(ui->pteDebug, ui->statusBar));
    apLogger->log("logger inited");
    
    settingsProcessor.setWin(this);
    
    readSettings();

    ssCommentPatterns.clear();
    
    ui->cbFilterCommentPattern->installEventFilter(this);
    

    
    rrTable.setTableWidget(ui->twTrans);
    
    ui->deFilterDateSelector->setDate(QDate::currentDate());

    setupConnections();
    
    statRender.setTables(
                ui->twStatAverage,
                ui->twStatTopTransPos,
                ui->twStatTopTransNeg,
                ui->twStatTopCatPos,
                ui->twStatTopCatNeg);
    statRender.setStatistic(&stater);
    
    rrChart.setDatabase(dbFiltered);
    rrChart.setChartWidgets(ui->wCatScat, ui->wSumOp);

    qslProExp << "[Expences]" << "[Profits]";
    
    checkButtonsEnability();
}


void CashTrash455MainWin::connectFilterCommentCombos(){
    connect(ui->cbFilterCat,    SIGNAL(currentIndexChanged(int)), 
            this, SLOT(actApplyFilter()));
    connect(ui->cbFilterScat,   SIGNAL(currentIndexChanged(int)), 
            this, SLOT(actApplyFilter()));
}

void CashTrash455MainWin::disconnectFilterCommentCombos(){
    disconnect(ui->cbFilterCat,    0, 0, 0);
    disconnect(ui->cbFilterScat,   0, 0, 0);
}

void CashTrash455MainWin::fillChartCombo(){
    disconnectChartCombo();

    ui->cbChartCat->clear();
    ui->cbChartCat->addItems(qslProExp);
    
    for (int i = 0; i < dbFiltered.getCats().size(); ++i) {
        ui->cbChartCat->addItem(dbFiltered.getCats().at(i));
    }

    connectChartCombo();
    updateChartCatScat();
}

void CashTrash455MainWin::connectChartCombo(){
    connect(ui->cbChartCat, SIGNAL(currentIndexChanged(int)),
            this, SLOT(updateChartCatScat()));
}

void CashTrash455MainWin::disconnectChartCombo(){
    disconnect(ui->cbChartCat, 0, 0, 0);
}

void CashTrash455MainWin::writeSettings(){
   settingsProcessor.writeSettings();
}

void CashTrash455MainWin::readSettings(){
    settingsProcessor.readAndApplySettings();
}

void CashTrash455MainWin::setupConnections(){
    connect(ui->pbClose,    SIGNAL(clicked()), 
            ui->actionClose, SLOT(trigger()));
    connect(ui->actionAddTransaction,   SIGNAL(triggered()), 
            this, SLOT(addTransaction()));
    connect(ui->actionOpen_Base,    SIGNAL(triggered()), 
            this, SLOT(readBase()));
    connect(ui->actionSave_base,    SIGNAL(triggered()), 
            this, SLOT(writeBase()));
    connect(ui->actionRemove_transaction, SIGNAL(triggered()),
            this, SLOT(actRemoveTransaction()));
    
    connect(ui->cbFilterSumOp,  SIGNAL(currentIndexChanged(int)), 
            this, SLOT(actApplyFilter()));
    connect(ui->dsbFilterSum,   SIGNAL(valueChanged(double)), 
            this, SLOT(actApplyFilter()));    
    connect(ui->cbFilterComment,    SIGNAL(currentIndexChanged(int)), 
            this, SLOT(actApplyFilter()));
    connect(ui->cbFilterCommentPattern,    SIGNAL(currentTextChanged(QString)), 
            this, SLOT(actApplyFilter()));
    connect(ui->cbFilterDateOp, SIGNAL(currentIndexChanged(QString)), 
            this, SLOT(actApplyFilter()));
    connect(ui->deFilterDateSelector,   SIGNAL(dateChanged(QDate)), 
            this, SLOT(actApplyFilter()));
    connect(ui->sbFilterDatePeriodCount,    SIGNAL(valueChanged(int)), 
            this, SLOT(actApplyFilter()));
    connect(ui->cbFiltefDatePeriodSelector, SIGNAL(currentIndexChanged(int)), 
            this, SLOT(actApplyFilter()));
    
    connect(ui->dsbInitBalance, SIGNAL(valueChanged(double)),
            this, SLOT(updateBaseInitBalance()));
    
    connect(ui->twTrans, SIGNAL(currentCellChanged(int,int,int,int)),
            this, SLOT(checkButtonsEnability()));
    
    connectFilterCommentCombos();
    connectChartCombo();
    
    connect(qApp, SIGNAL(aboutToQuit()), 
            this, SLOT(actOnClose()));
}


CashTrash455MainWin::~CashTrash455MainWin() {
    delete ui;
}

bool CashTrash455MainWin::isFilterVisible(){
    return ui->gbFilter->isVisible();
}

bool CashTrash455MainWin::isLogVisible(){
    return ui->pteDebug->isVisible();
}

bool CashTrash455MainWin::isBaseInfoVisible(){
    return ui->gbInfo->isVisible();
}

void CashTrash455MainWin::setFilterVisiblility(bool visibility){
    ui->actionToggle_filter->setChecked(visibility);
    ui->gbFilter->setVisible(visibility);
}

void CashTrash455MainWin::setLogVisiblility(bool visibility){
    ui->actionToggleLog->setChecked(visibility);
    ui->pteDebug->setVisible(visibility);    
}

void CashTrash455MainWin::setBaseInfoVisiblility(bool visibility){
    ui->actionToggle_base_info->setChecked(visibility);
    ui->gbInfo->setVisible(visibility);        
}

int CashTrash455MainWin::currentTabOpened(){
    return ui->tabw->currentIndex();
}

void CashTrash455MainWin::setCurrentTab(const int tabIndex){
    ui->tabw->setCurrentIndex(tabIndex);
}

void CashTrash455MainWin::closeApp() {
    writeSettings();

    close();
}

void CashTrash455MainWin::actApplyFilter(){
    if(db.size()){
        apLogger->log("grabbing filter data");
        grabFilterData();
        renderBase();
    }
}

void CashTrash455MainWin::actToggleFilter(){
    ui->gbFilter->setVisible(!ui->gbFilter->isVisible());
    ui->actionToggle_filter->setChecked(ui->gbFilter->isVisible());
    qDebug() << "act toggle filterr: flt vis :" << ui->gbFilter->isVisible();
}

void CashTrash455MainWin::actToggleLog(){
    ui->pteDebug->setVisible(!ui->pteDebug->isVisible());
    ui->actionToggleLog->setChecked(ui->pteDebug->isVisible());
}

void CashTrash455MainWin::actToggleBaseInfo(){
    ui->gbInfo->setVisible(!ui->gbInfo->isVisible());
    ui->actionToggle_base_info->setChecked(ui->gbInfo->isVisible());
}

void CashTrash455MainWin::on_actionClose_triggered() {
    closeApp();
}

void CashTrash455MainWin::fooFillBase() {
    db.setInitBalance(200.20);

    t.setData("food", 33.5, "lock", "first", QDate::currentDate());
    qDebug() << "isvalid\t" << t.isValid();
    qDebug() << t.toString();
    db.addTransaction(t);

    t.setData("food", -25.2, "gipp");
    qDebug() << "isvalid\t" << t.isValid();
    qDebug() << t.toString();
    db.addTransaction(t);

    t.setData("salary", 550.55, "mkb", "big salary");
    qDebug() << "isvalid\t" << t.isValid();
    qDebug() << t.toString();
    db.addTransaction(t);

    qDebug() << "base contains" << db.size();
    db.recalculate();    
}

void CashTrash455MainWin::fooFillFilter(){
    filter.setDatePeriod(dpYear);
    filter.setDatePreiodCount(2);
}

void CashTrash455MainWin::on_pbActX_clicked() {
    ui->actionToggle_filter->setChecked(ui->gbFilter->isVisible());
}

void CashTrash455MainWin::on_pbActA_clicked() {
    writeBase();
}

void CashTrash455MainWin::rrBaseInfo(){
    ui->lCurrBalance->setText(QString::number(dbFiltered.getCurrentBalance()));    
    ui->lTransVisible->setText(QString("%1/%2").arg(dbFiltered.size()).arg(db.size()));
    ui->dsbInitBalance->setValue(db.getInitBalance());
    ui->lProfit->setText(QString("%1").arg(stater.report.topPMCats.topPSum()));
    ui->lExpence->setText(QString("%1").arg(stater.report.topPMCats.topMSum()));
}

void CashTrash455MainWin::renderBase() {
    dbFiltered = Filtrator::filtrate(filter, db);
    dbFiltered.recalculate();

    rrText.setDatabase(dbFiltered);
    rrText.render();
    ui->pteDebug->appendPlainText(rrText.reportString());
        
    rrTable.setDatabase(dbFiltered);
    rrTable.render();
    
    rrChart.setDatabase(dbFiltered);
    rrChart.renderSumOp();
    
    actStatAnalize();    
    
    fillFilterCatScat();
    fillChartCombo();
    
    rrBaseInfo();
    checkButtonsEnability();
}

void CashTrash455MainWin::writeBase() {
    apLogger->log("writting base");
    db.setInitBalance(ui->dsbInitBalance->value());
    ioXml::writeToXml(databaseFileName, db);
}

void CashTrash455MainWin::on_pbActiB_clicked() {
    readBase();
}

void CashTrash455MainWin::readBase() {
    apLogger->log("reading base...");
    db = ioXml::readFromXml(databaseFileName);
    db.recalculate();
    renderBase();    
    apLogger->log("\t...done");
}

void CashTrash455MainWin::on_paAnal_clicked() {
    statAnalyze();
}

void CashTrash455MainWin::actStatAnalize()
{
    stater.analyzeDatabase(&dbFiltered);
    apLogger->log(stater.report.toString());
    statRender.render();    
}

void CashTrash455MainWin::updateChartCatScat(){
    qDebug() << (QString("refresh category:\t[%1]").arg(ui->cbChartCat->currentText()));
    Common::scatList_t list;
    double catsum;
    
    if(qslProExp.contains(ui->cbChartCat->currentText())){
        if(ui->cbChartCat->currentText() == qslProExp.at(0)){
            list = stater.report.topPMCats.topMCats;
            catsum = stater.report.topPMCats.topMSum();
        }else{
            list = stater.report.topPMCats.topPCats;
            catsum = stater.report.topPMCats.topPSum();
        }
    }else{
        stater.report.catScat.generate(&dbFiltered, ui->cbChartCat->currentText());
        
        list = stater.report.catScat.scatList();
        catsum = stater.report.catScat.catSum();
    }
    
    rrChart.renderCatScat(list, catsum);
    qDebug() << "list to render sz:" << list.size() << " " << catsum;
}

void CashTrash455MainWin::actOnClose(){
    writeBase();
}

void CashTrash455MainWin::updateBaseInitBalance(){
    db.setInitBalance(ui->dsbInitBalance->value());
    renderBase();
}

void CashTrash455MainWin::checkButtonsEnability(){
    bool rowSelected = ui->twTrans->currentRow() != -1;
    ui->actionRemove_transaction->setEnabled(rowSelected);
    ui->actionEditTransaction->setEnabled(rowSelected);
}

void CashTrash455MainWin::actRemoveTransaction(){
    if(ui->twTrans->currentRow() != -1){
        int invertedIndex = ui->twTrans->rowCount() - ui->twTrans->currentRow() - 1;
        db.removeTransaction(dbFiltered.item(invertedIndex));
    }
    renderBase();
}

void CashTrash455MainWin::statAnalyze() {
    apLogger->log("--==report==--");

    actStatAnalize();
    
    apLogger->log("\t...done");
}

void CashTrash455MainWin::on_pbAddTransaction_clicked() {
    addTransaction();
}

void CashTrash455MainWin::addTransaction() {
    apLogger->log("attempt to add new transaction");
    dlgEditTransaction* newTrans = new dlgEditTransaction(this);
    if(newTrans->exec() == QDialog::Accepted){
        apLogger->log("new trans accepted");
        apLogger->log(newTrans->getTransaction().toString());
        db.addTransaction(newTrans->getTransaction());
        renderBase();
    }else{
        apLogger->log("new trans rejected");
    }
}

void CashTrash455MainWin::grabFilterData(){
    filter.clear();
    
    if(ui->cbFilterSumOp->currentIndex()){
        filter.setSum(ui->dsbFilterSum->value());
        filter.setSumOp(commonOp_e(ui->cbFilterSumOp->currentIndex()));
    }
    
    if(ui->cbFilterCat->currentIndex()){
        filter.setCat(ui->cbFilterCat->currentText());
    }
    
    if(ui->cbFilterScat->currentIndex()){
        filter.setScat(ui->cbFilterScat->currentText());
    }
    
    if(ui->cbFilterComment->currentIndex()){
        filter.setCommentOp(textOp_e(ui->cbFilterComment->currentIndex()));
        filter.setCommentPattern(ui->cbFilterCommentPattern->currentText());
    }
    
    if(ui->cbFilterDateOp->currentIndex()){
        filter.setDateOp(commonOp_e(ui->cbFilterDateOp->currentIndex()));
        filter.setDate(ui->deFilterDateSelector->date());
    }
    
    if(ui->cbFiltefDatePeriodSelector->currentIndex()){
        filter.setDatePeriod(datePeriod_e(ui->cbFiltefDatePeriodSelector->currentIndex()));
        filter.setDatePreiodCount(ui->sbFilterDatePeriodCount->value());
    }
}

void CashTrash455MainWin::renderFilter(){
    ui->dsbFilterSum->setValue(filter.sum());
    ui->cbFilterSumOp->setCurrentIndex(filter.sumOp());
    
    ui->cbFilterCat->setCurrentText(filter.cat());
    ui->cbFilterScat->setCurrentText(filter.scat());
    
    ui->cbFilterComment->setCurrentIndex(filter.commentOp());
    ui->cbFilterCommentPattern->setCurrentText(filter.commentPattern());
    
    ui->cbFilterDateOp->setCurrentIndex(filter.dateOp());
    ui->deFilterDateSelector->setDate(filter.getDate());
    ui->cbFiltefDatePeriodSelector->setCurrentIndex(filter.datePeriod());
    ui->sbFilterDatePeriodCount->setValue(filter.getDatePreiodCount());
}

bool CashTrash455MainWin::eventFilter(QObject *o, QEvent *e)
{
    if(e->type() == QEvent::FocusOut 
            && o == ui->cbFilterCommentPattern){
        ssCommentPatterns.insert(ui->cbFilterCommentPattern->currentText());
        ui->cbFilterCommentPattern->clear();
        ui->cbFilterCommentPattern->addItems(ssCommentPatterns.toList());
    }
    
    return false;
}

void CashTrash455MainWin::fillFilterCatScat()
{
    disconnectFilterCommentCombos();
    
    QString buff;
    
    buff = ui->cbFilterCat->currentText();    
    ui->cbFilterCat->clear();
    ui->cbFilterCat->addItem("");
    ui->cbFilterCat->addItems(dbFiltered.getCats());
    if(!buff.isEmpty()){
        ui->cbFilterCat->setCurrentText(buff);
    }
    
    buff = ui->cbFilterScat->currentText();
    ui->cbFilterScat->clear();
    ui->cbFilterScat->addItem("");
    ui->cbFilterScat->addItems(dbFiltered.getScats());
    if(!buff.isEmpty()){
        ui->cbFilterScat->setCurrentText(buff);
    }    
    
    connectFilterCommentCombos();
}

void CashTrash455MainWin::on_pbFilterClear_clicked(){
    filter.clear();
    renderFilter();
}

void CashTrash455MainWin::on_actionToggle_filter_triggered(){
    actToggleFilter();
}

void CashTrash455MainWin::on_actionToggleLog_triggered(){
    actToggleLog();
}

void CashTrash455MainWin::on_actionToggle_base_info_triggered(){
    actToggleBaseInfo();
}

void CashTrash455MainWin::on_actionSave_base_triggered(){
    
}
