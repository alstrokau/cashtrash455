#ifndef DLGEDITTRANSACTION_H
#define DLGEDITTRANSACTION_H

#include "ds/transaction.h"
#include <QDialog>

namespace Ui {
class dlgEditTransaction;
}

class dlgEditTransaction : public QDialog
{
    Q_OBJECT
    
public:
    explicit dlgEditTransaction(QWidget *parent = 0);
    ~dlgEditTransaction();
    Transaction& getTransaction();
    
private slots:
    void getDataToTransaction();
    
private:
    Ui::dlgEditTransaction *ui;
    Transaction tr_;
};

#endif // DLGEDITTRANSACTION_H
