#ifndef CASHTRASH455MAINWIN_H
#define CASHTRASH455MAINWIN_H

#include <QMainWindow>
#include <QDebug>
#include <memory>

#include "ds/transaction.h"
#include "ds/database.h"
#include "rr/textrenderer.h"
#include "rr/tablerenderer.h"
#include "io/ioxml.h"
#include "st/statistic.h"
#include "tl/pteLogger/ptelogger.h"
#include "filter/filter.h"
#include "filter/filtrator.h"
#include "rr/stattablerenderer.h"
#include "rr/chartsrenderer.h"
#include "tl/SettingsProcessor/settingsprocessor.h"

namespace Ui
{
class CashTrash455MainWin;
}

static const QString databaseFileName = "base.ctb.xml";

class CashTrash455MainWin : public QMainWindow
{
    Q_OBJECT

public:
    explicit CashTrash455MainWin(QWidget *parent = 0);
    ~CashTrash455MainWin();
    
    bool isFilterVisible();
    void setFilterVisiblility(bool visibility = true);
    bool isLogVisible();
    void setLogVisiblility(bool visibility = true);
    bool isBaseInfoVisible();
    void setBaseInfoVisiblility(bool visibility = true);
    
    int currentTabOpened();
    void setCurrentTab(const int tabIndex);
    
        
private slots:
    void fooFillBase();
    void fooFillFilter();
    void readBase();
    void writeBase();
    void statAnalyze();
    void addTransaction();
    void renderBase();
    void closeApp();
    void actApplyFilter();
    void actToggleFilter();
    void actToggleLog();
    void actToggleBaseInfo();
    void setupConnections();
    void rrBaseInfo();
    void actStatAnalize();
    void updateChartCatScat();
    void actOnClose();
    void updateBaseInitBalance();
    void checkButtonsEnability();
    void actRemoveTransaction();

    void on_actionClose_triggered();
    void on_pbActX_clicked();
    void on_pbActA_clicked();
    void on_pbActiB_clicked();
    void on_paAnal_clicked();
    void on_pbAddTransaction_clicked();
    void on_pbFilterClear_clicked();    
    void on_actionToggle_filter_triggered();
    void on_actionToggleLog_triggered();
    void on_actionToggle_base_info_triggered();
    
    void on_actionSave_base_triggered();
    
private:
    Ui::CashTrash455MainWin *ui;

    Transaction t;
    Database db;
    Database dbFiltered;
    QSet<QString> ssCommentPatterns;

    TextRenderer rrText;
    TableRenderer rrTable;
    ChartsRenderer rrChart;
//    ioXml ioX;
    Statistic stater;
    StatTableRenderer statRender;
    std::auto_ptr<pteLogger> apLogger;
    Filter filter;
//    Filtrator filtrator;
    QStringList qslProExp;
    SettingsProcessor settingsProcessor;
    
    void grabFilterData();
    void renderFilter();
    bool eventFilter(QObject *o, QEvent *e);
    void fillFilterCatScat();
    void connectFilterCommentCombos();
    void disconnectFilterCommentCombos();
    void fillChartCombo();
    void connectChartCombo();
    void disconnectChartCombo();
    void writeSettings();
    void readSettings();
};

#endif // CASHTRASH455MAINWIN_H
