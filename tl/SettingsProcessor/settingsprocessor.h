#ifndef SETTINGSPROCESSOR_H
#define SETTINGSPROCESSOR_H

#include <QString>

class CashTrash455MainWin;

static const QString settingsFileName = "settings.ini";

class SettingsProcessor
{
public:
    SettingsProcessor();
    void writeSettings();
    void readAndApplySettings();
    void setWin(CashTrash455MainWin *windows = NULL);
    
private:
    CashTrash455MainWin* win_;
};

#endif // SETTINGSPROCESSOR_H
