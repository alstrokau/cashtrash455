#include "settingsprocessor.h"
#include "../cashtrash455/win/cashtrash455mainwin.h"
#include <QSettings>

SettingsProcessor::SettingsProcessor():
    win_(NULL){
}

void SettingsProcessor::writeSettings(){
    qDebug() << "writting settings";
    
    QSettings sett(settingsFileName, QSettings::IniFormat);
    if(win_ !=  NULL){
        sett.setValue("geometry", win_->saveGeometry());
        sett.setValue("state", win_->saveState());
        sett.setValue("isFilterVisible", win_->isFilterVisible());
        sett.setValue("isLogVisible", win_->isLogVisible());
        sett.setValue("isBaseInfoVisible", win_->isBaseInfoVisible());
        sett.setValue("currentTab", win_->currentTabOpened());
    }
}

void SettingsProcessor::readAndApplySettings(){
    qDebug() << "reading settings";
    
    QSettings sett(settingsFileName, QSettings::IniFormat);
    if(win_ !=  NULL){
        win_->restoreGeometry(
                    sett.value("geometry").toByteArray());
        win_->restoreState(
                    sett.value("state").toByteArray());
        win_->setFilterVisiblility(
                    sett.value("isFilterVisible", false).toBool());
        win_->setLogVisiblility(
                    sett.value("isLogVisible", false).toBool());
        win_->setBaseInfoVisiblility(
                    sett.value("isBaseInfoVisible", true).toBool());
        win_->setCurrentTab(
                    sett.value("currentTab", 0).toInt());
    }
}

void SettingsProcessor::setWin(CashTrash455MainWin *windows){
    win_ = windows;
}
