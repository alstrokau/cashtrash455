#include "ptelogger.h"

static const QString logTimestampFormat = "hh:mm:ss.zzz";

pteLogger::pteLogger(QPlainTextEdit *pte, QStatusBar *sb):
    pte_(pte), sb_(sb)
{
}

void pteLogger::log(const QString &message)
{
    static QTime prevTime;
    QString msg(QString("%1 (%2):\t%3")
        .arg(QTime::currentTime().toString(logTimestampFormat))
        .arg(prevTime.msecsTo(QTime::currentTime()))
        .arg(message));
    
    if(pte_ != NULL){
        pte_->appendPlainText(msg);
    }
    
    if(sb_ != NULL){
		sb_->showMessage(msg, 5000);
    }
    prevTime = QTime::currentTime(); 
}
