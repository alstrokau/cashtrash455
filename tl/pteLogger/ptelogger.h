#ifndef PTELOGGER_H
#define PTELOGGER_H

#include <QPlainTextEdit>
#include <QStatusBar>
#include <QDateTime>

class pteLogger
{
public:
    pteLogger(QPlainTextEdit* pte, QStatusBar* sb = NULL);
    void log(const QString& message);
    
private:
    pteLogger();
    QPlainTextEdit* pte_;
    QStatusBar* sb_;
};

#endif // PTELOGGER_H
