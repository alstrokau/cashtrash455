#include "tablerenderer.h"
#include <QScrollBar>
#include <QHeaderView>

TableRenderer::TableRenderer() :
    tableWidget_(NULL)
{}

TableRenderer::TableRenderer(QTableWidget *tableWidget) :
    tableWidget_(tableWidget)
{
    
}

void TableRenderer::setTableWidget(QTableWidget *tableWidget)
{
    tableWidget_ = tableWidget;
}

void TableRenderer::render()
{
    tableWidget_->setRowCount(db_.size());
    tableWidget_->setSelectionBehavior(QAbstractItemView::SelectRows);
    
    int row = 0;

    for(int i = db_.size() - 1; i >= 0; i--){
        QTableWidgetItem *wi;

        wi = new QTableWidgetItem(db_.item(i).cat());
        wi->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        tableWidget_->setItem(row, 0, wi);
        
        tableWidget_->setItem(row, 1,
            new QTableWidgetItem(db_.item(i).scat()));
        
        wi = new QTableWidgetItem(QString::number(db_.item(i).sum()));
        wi->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        if(db_.item(i).sum() > 0){
            wi->setBackgroundColor(qRgb(220, 255, 220));
        }else{
            wi->setBackgroundColor(qRgb(255, 220, 220));
        }        
        tableWidget_->setItem(row, 2, wi);
        
        wi = new QTableWidgetItem(db_.item(i).datetime().toString("dd.MM.yyyy"));
        wi->setTextAlignment(Qt::AlignCenter);
        tableWidget_->setItem(row, 3, wi);

        wi = new QTableWidgetItem(db_.item(i).comment());
        if(db_.item(i).comment().contains("[plan]")){
            wi->setBackgroundColor(QColor(200, 200, 200));
        }
        tableWidget_->setItem(row, 4, wi);
        
        wi = new QTableWidgetItem(QString::number(db_.item(i).balance()));
        wi->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        tableWidget_->setItem(row, 5, wi);

        row++;
    }
    
    tableWidget_->resizeColumnsToContents();
    tableWidget_->resizeRowsToContents();
    tableWidget_->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
}

void TableRenderer::setupTableWidget()
{
    tableWidget_->setRowCount(1);
}
