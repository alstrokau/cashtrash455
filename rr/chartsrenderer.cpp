#include "chartsrenderer.h"
#include <QFont>

ChartsRenderer::ChartsRenderer(){}

void ChartsRenderer::setChartWidgets(QCustomPlot *catScat, QCustomPlot *SumOp){
    cpCatScat_ = catScat;
    cpSumOp_ = SumOp;
}

void ChartsRenderer::render(){
    
}

void ChartsRenderer::renderCatScat(const Common::scatList_t subcats, const double catsum){
    if(cpCatScat_ == NULL){
        qDebug() << "catscat chart widget is null";
        return;
    }
    
    QList<QCPBars*> lb;
    qsrand(QTime::currentTime().msecsSinceStartOfDay());
    cpCatScat_->clearPlottables();
    
    for(int i = 0; i < subcats.size(); i++){
        QCPBars *barsBuff = new QCPBars(cpCatScat_->xAxis, cpCatScat_->yAxis);
        lb.push_back(barsBuff);
        cpCatScat_->addPlottable(barsBuff);
        QVector<double> key;
        key << 1.0;
        QVector<double> value;
        value << subcats.at(i).second;
        barsBuff->setData(key, value);
        barsBuff->setName(QString("[%2%]%1").arg(subcats.at(i).first).arg(QString::number(subcats.at(i).second / catsum * 100.0, 'f', 1)));
        QPen pen;
        pen.setWidthF(2.0);
        
        byte r = 10 + qrand()%200;
        byte g = 10 + qrand()%200;
        byte b = 10 + qrand()%200;
        pen.setColor(QColor(r, g, b));
        barsBuff->setBrush(QColor(r, g, b, 60));
        barsBuff->setPen(pen);
        if(i > 0){
            if(subcats.at(i).second > 0){
                barsBuff->moveBelow(lb.at(i - 1));
            }else{
                barsBuff->moveAbove(lb.at(i - 1));
            }
        }
    }
    
    //cpCatScat_->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    cpCatScat_->xAxis->setRange(0.5, 2.0);
    if(catsum > 0){
        cpCatScat_->yAxis->setRange(0.0, catsum * 1.1);
    }else{
        cpCatScat_->yAxis->setRange(catsum * 1.1, 0.0);
    }
        
    cpCatScat_->legend->setVisible(true);
    cpCatScat_->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignRight);
    cpCatScat_->legend->setBrush(QColor(255, 255, 255, 200));
    QPen legendPen;
    legendPen.setColor(QColor(130, 130, 130, 200));
    cpCatScat_->legend->setBorderPen(legendPen);
    QFont legendFont = cpCatScat_->legend->font();
    legendFont.setPointSize(10);
    cpCatScat_->legend->setFont(legendFont);
    cpCatScat_->replot();
}

void ChartsRenderer::renderSumOp(){
    if(cpSumOp_ == NULL){
        qDebug() << "sumop chart widget is null";
        return;
    }
    
    qDebug() << "sumop plot; dbsize:" << db_.size();
    
    cpSumOp_->addGraph();
    QVector<double> k;
    QVector<double> v;
    double maxBalance = 0.0;
    double minBalance = 0.0;
    double currentBalance = 0.0;
    for (int i = 0; i < db_.size(); ++i) {
        k.push_back(-db_.item(i).datetime().daysTo(QDate::currentDate()));
        currentBalance = db_.item(i).balance();
        v.push_back(currentBalance);
        if(maxBalance < currentBalance){
            maxBalance = currentBalance;
        }
        if(minBalance > currentBalance){
            minBalance = currentBalance;
        }
    }
    cpSumOp_->graph()->setData(k, v);
    cpSumOp_->graph()->rescaleAxes();
    cpSumOp_->graph()->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 10));
    
    cpSumOp_->yAxis->setRange(minBalance, maxBalance * 1.1);
    
    
    QPen pen(QColor(100, 200, 100));
    pen.setWidthF(2.8);
    cpSumOp_->graph()->setPen(pen);
    cpSumOp_->graph()->setBrush(QBrush(QColor(100, 200, 100, 70)));
    cpSumOp_->replot();
    qDebug() << "sumop plot done";
}
