#include "textrenderer.h"

TextRenderer::TextRenderer() : 
    reportString_("")
{
}

void TextRenderer::render()
{
    reportString_ = "--==textrenderer==--\n";
    QString sTransactionLine;
    for (int c = 0; c < db_.size(); c++) {
        sTransactionLine = "";
        sTransactionLine += QString("#%1 | %2").arg(c, 4).arg(db_.item(c).toString());
        reportString_ += sTransactionLine;
        reportString_ += "\n";
    }
    
    reportString_ += QString( "-------------------------------------\n"
				      "init balance:\t%1\ncurr balance:\t%2\ntransactions:\t%3\n" \
					  "-------------------------------------\n")
                  .arg(db_.getInitBalance())
                  .arg(db_.getCurrentBalance())
                  .arg(db_.size());
}

QString TextRenderer::reportString() const
{
    return reportString_;
}
