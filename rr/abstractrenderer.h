#ifndef ABSTRACTRENDERER_H
#define ABSTRACTRENDERER_H

#include "ds/database.h"

class AbstractRenderer
{
public:
    AbstractRenderer();
    AbstractRenderer(const Database database);
    virtual ~AbstractRenderer();

    void setDatabase(const Database database);
    virtual void render() = 0;
    

protected:
    Database db_;
};

#endif // ABSTRACTRENDERER_H
