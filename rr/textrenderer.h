#ifndef TEXTRENDERER_H
#define TEXTRENDERER_H

#include "rr/abstractrenderer.h"
#include "ds/database.h"

class TextRenderer : public AbstractRenderer
{
public:
    TextRenderer();    
    void render();
    
    QString reportString() const;    
    
private:
    QString reportString_;
};

#endif // TEXTRENDERER_H
