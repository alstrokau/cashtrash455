#ifndef CHARTSRENDERER_H
#define CHARTSRENDERER_H

#include "rr/abstractrenderer.h"
#include "ds/common.h"
//#include "st/statistic.h"
#include "qcustomplot/qcustomplot.h"

class ChartsRenderer : public AbstractRenderer
{
public:
    ChartsRenderer();
    void setChartWidgets(QCustomPlot *catScat, QCustomPlot *SumOp);
    void render();
    void renderCatScat(const Common::scatList_t subcats, const double catsum);
    void renderSumOp();
    
    
private:
    QCustomPlot *cpCatScat_;
    QCustomPlot *cpSumOp_;
};

#endif // CHARTSRENDERER_H
