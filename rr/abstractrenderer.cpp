#include "abstractrenderer.h"

AbstractRenderer::AbstractRenderer()
{
}

AbstractRenderer::AbstractRenderer(const Database database)
{
    setDatabase(database);
}

AbstractRenderer::~AbstractRenderer()
{
    
}

void AbstractRenderer::setDatabase(const Database database)
{
    db_ = database;
}
