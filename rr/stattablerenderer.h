#ifndef STATTABLERENDERER_H
#define STATTABLERENDERER_H

#include "rr/abstractrenderer.h"
#include "st/statistic.h"
#include <QTableWidget>

class StatTableRenderer : public AbstractRenderer, public QObject
{
public:
    StatTableRenderer();
    void setTables(QTableWidget *tableAverage = NULL,
                   QTableWidget *tableTopTransPos = NULL,
                   QTableWidget *tableTopTransNeg = NULL,
                   QTableWidget *tableTopCatPos = NULL,
                   QTableWidget *tableTopCatNeg = NULL);
    void setStatistic(Statistic *statistic = NULL);
    
    void render();
        
private:
    QTableWidget *twAv_;
    QTableWidget *twTopTransPos_;
    QTableWidget *twTopTransNeg_;
    QTableWidget *twTopCatPos_;
    QTableWidget *twTopCatNeg_;
    Statistic *statistic_;
    
    void resizeTablesToContent();
    void renderAverage();
    void renderTopCatPos();
    void renderTopCatNeg();
    void renderTopTransPos();
    void renderTopTransNeg();
};

#endif // STATTABLERENDERER_H
