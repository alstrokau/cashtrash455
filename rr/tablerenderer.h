#ifndef TABLERENDERER_H
#define TABLERENDERER_H

#include "rr/abstractrenderer.h"
#include <QTableWidget>

class TableRenderer : public AbstractRenderer
{
public:
    TableRenderer();    
    TableRenderer(QTableWidget *tableWidget);
    
    void setTableWidget(QTableWidget *tableWidget);
    void render();

protected:
    QTableWidget *tableWidget_;
    
    void setupTableWidget();
};

#endif // TABLERENDERER_H
