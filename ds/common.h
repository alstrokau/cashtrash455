#ifndef COMMON_H
#define COMMON_H

#include <QPair>
#include <QList>

class Common
{   
public:
    Common();
    
    typedef QPair<QString, double> p_sd_t;
    typedef QList<p_sd_t> scatList_t;
};

#endif // COMMON_H
