#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <QString>
#include <QDate>

class Transaction
{
public:
    Transaction();
    Transaction(QString cat,
                double sum = 0.0,
                QString scat = "",
                QString comment = "",
                QDate datetime = QDate::currentDate());

    QString cat() const;
    void setCat(const QString &cat);

    QString scat() const;
    void setScat(const QString &scat);

    QDate datetime() const;
    void setDatetime(const QDate &datetime);

    double balance() const;
    void setBalance(double balance);

    double sum() const;
    void setSum(double sum);

    QString comment() const;
    void setComment(const QString &comment);

    void setData(QString cat,
                double sum = 0.0,
                QString scat = "",
                QString comment = "",
                QDate datetime = QDate::currentDate());

    bool isValid() const;
    QString toString() const;
    bool operator ==(const Transaction& other) const;

private:
    QString cat_;
    double sum_;
    QString scat_;
    QString comment_;
    QDate datetime_;
    
    double balance_;
    bool isValid_;

    void validate();
};

#endif // TRANSACTION_H
