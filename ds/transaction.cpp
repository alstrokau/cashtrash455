#include "transaction.h"

QString Transaction::cat() const
{
    return cat_;
}

QString Transaction::comment() const
{
    return comment_;
}

void Transaction::setComment(const QString &comment)
{
    comment_ = comment;
}

void Transaction::setData(QString cat, double sum, QString scat, QString comment, QDate datetime)
{
    setCat(cat);
    setScat(scat);
    setSum(sum);
    setDatetime(datetime);
    setComment(comment);
    validate();
}

Transaction::Transaction(QString cat, double sum, QString scat, QString comment, QDate datetime){
    setData(cat, sum, scat, comment, datetime);
    balance_ = 0;
}

bool Transaction::isValid() const
{
    return isValid_;
}

QString Transaction::toString() const
{
    QString rez = QString("[%1:%2]\t%3\t%4\t@%5\t//%6")
                  .arg(cat_)
                  .arg(scat_)
                  .arg(balance_)
                  .arg(sum_)
                  .arg(datetime_.toString("[dd.MM.yyyy]"))
                  .arg(comment_);

    return rez;
}

bool Transaction::operator ==(const Transaction &other) const{
    bool rez;
    rez = ( cat_ == other.cat() &&
            scat_ == other.scat() &&
            sum_ == other.sum() &&
            datetime_ == other.datetime() &&
            comment_ == other.comment() );
    return rez;
}

void Transaction::validate()
{
    isValid_ = (!cat_.isEmpty()) && (!scat_.isEmpty()) && (sum_ != 0.0);
}

double Transaction::sum() const
{
    return sum_;
}

void Transaction::setSum(double sum)
{
    if (sum_ != sum) {
        sum_ = sum;
    }

    validate();
}

double Transaction::balance() const
{
    return balance_;
}

void Transaction::setBalance(double balance)
{
    if (balance_ != balance) {
        balance_ = balance;
    }
}

QDate Transaction::datetime() const
{
    return datetime_;
}

void Transaction::setDatetime(const QDate &datetime)
{
    if (datetime_ != datetime) {
        datetime_ = datetime;
    }
}

QString Transaction::scat() const
{
    return scat_;
}

void Transaction::setScat(const QString &scat)
{
    if (scat_ != scat) {
        scat_ = scat;
    }

    validate();
}

void Transaction::setCat(const QString &cat)
{
    if (cat_ != cat) {
        cat_ = cat;
    }

    validate();
}


Transaction::Transaction():
    cat_(""),
    sum_(0.0),
    scat_(""),
    comment_(""),
    datetime_(QDate::currentDate()),
    balance_(0.0),
    isValid_(false)
{
}
