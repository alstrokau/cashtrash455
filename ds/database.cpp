#include "database.h"
#include <QStringList>

Database::Database():
    initBalance_(0.0),
    currentBalance_(0.0)
{
}

void Database::addTransaction(const Transaction &transaction)
{
    if (transaction.isValid()) {
        transactions_.append(transaction);
        recalculate();
    } else {
        qDebug() << "Transaction is not valid";
    }
}

int Database::size() const
{
    return transactions_.size();
}

void Database::removeTransaction(const uint index)
{
    if (isIndexValid(index)) {
        transactions_.removeAt(index);
    }
}

void Database::removeTransaction(const Transaction& transaction){
    transactions_.removeAll(transaction);
}

bool Database::isIndexValid(const int index) const
{
    return (index >= 0 && index < transactions_.size());
}

double Database::getInitBalance() const
{
    return initBalance_;
}

void Database::setInitBalance(double value)
{
    if (initBalance_ != value) {
        initBalance_ = value;
    }
}

double Database::getCurrentBalance() const
{
    return currentBalance_;
}

void Database::recalculate()
{
    double val = initBalance_;

    for (int c = 0; c < transactions_.size(); c++) {
        val += transactions_.at(c).sum();
        transactions_[c].setBalance(val);
    }

    currentBalance_ = val;
}

QString Database::baseInfo() const
{
    QString rez = QString("ib:%1\ncb:%2\nsz:%3")
                  .arg(initBalance_)
                  .arg(currentBalance_)
                  .arg(size());

    return rez;
}

Transaction Database::item(const int index) const
{
    if (isIndexValid(index)) {
        return transactions_.at(index);
    } else {
        return Transaction();
    }
}

void Database::clear()
{
    initBalance_ = 0.0;
    currentBalance_ = 0.0;
    transactions_.clear();
}

QStringList Database::getCats(const FinanceType type) const
{
    QSet<QString> ssBuffer;
    
    switch (type) {
    case ftAll:
        for (int i = 0; i < transactions_.size(); ++i) {
            ssBuffer.insert(transactions_.at(i).cat());
        }
        break;
    case ftProfit:
        for (int i = 0; i < transactions_.size(); ++i) {
            if (transactions_.at(i).sum() > 0.0) {
                ssBuffer.insert(transactions_.at(i).cat());
            }
        }
        break;
    case ftExpence:
        for (int i = 0; i < transactions_.size(); ++i) {
            if (transactions_.at(i).sum() < 0.0) {
                ssBuffer.insert(transactions_.at(i).cat());
            }
        }
        break;
    default:
        break;
    };
    
    return ssBuffer.toList();
}

QStringList Database::getScats(const QString &category) const
{
    QSet<QString> ssBuffer;
    
    for (int i = 0; i < transactions_.size(); ++i) {
        if(category.isEmpty()){
            ssBuffer.insert(transactions_.at(i).scat());
        }else{
            if(transactions_.at(i).cat() == category){
                ssBuffer.insert(transactions_.at(i).scat());
            }
        }
    }

    return ssBuffer.toList();
}
