#ifndef STATTOPPMTRANS_H
#define STATTOPPMTRANS_H

#include "ds/database.h"
#include "st/abstractstatmodule.h"

class StatTopPMTrans : public AbstractStatModule
{
public:
    StatTopPMTrans();
    virtual void generate(const Database *database);
    virtual QString toString();
    QString info();
    
    TransactionList_t topPTrans();
    TransactionList_t topMTrans();
    
private:
    TransactionList_t topPTrans_;
    TransactionList_t topMTrans_;
    TransactionList_t sortedTransactions_;
};

#endif // STATTOPPMTRANS_H
