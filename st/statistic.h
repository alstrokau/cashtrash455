#ifndef STATISTIC_H
#define STATISTIC_H

#include "ds/database.h"
#include "st/statisticreport.h"

class Statistic
{
public:
    Statistic();
    void analyzeDatabase(const Database *database);
    StatisticReport report;


private:
    const Database *database_;
    void analyze();
};

#endif // STATISTIC_H
