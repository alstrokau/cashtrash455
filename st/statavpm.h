#ifndef STATAVPM_H
#define STATAVPM_H

#include "ds/database.h"
#include "ds/transaction.h"
#include "st/abstractstatmodule.h"
#include <QString>

class StatAvPM : public AbstractStatModule
{
public:
    StatAvPM();
    void generate(const Database *database);
    QString toString();
    QString info();
    
    double avP() const;
    double avM() const;
    double avPw() const;
    double avMw() const;
    double avPm() const;
    double avMm() const;
    double avPy() const;
    double avMy() const;
    
private:
    //const Database *db_;
    double avP_;
    double avM_;
    double avP_w;
    double avM_w;
    double avP_m;
    double avM_m;
    double avP_y;
    double avM_y;
    int datesCount_;
};

#endif // STATAVPM_H
