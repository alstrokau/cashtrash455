#include "stattoppmcats.h"

StatTopPMCats::StatTopPMCats() :
    topPSum_(0.0),
    topMSum_(0.0){
}

void StatTopPMCats::generate(const Database *database){
    QMap<QString, double> topCats;
    clearTopCats();
    generateCatSums(database, topCats);
    splitCatSumsIntoPMCats(topCats);
    sortTopPMCats();
}

void StatTopPMCats::clearTopCats(){
    topPCats.clear();
    topMCats.clear();
    topPSum_ = 0.0;
    topMSum_ = 0.0;
}

void StatTopPMCats::generateCatSums(const Database *database, QMap<QString, double> &topCats){
    Transaction currItem;
    for (int i = 0; i < database->size(); i++) {
        currItem = database->item(i);
        topCats[currItem.cat()] += currItem.sum();
        
        if(currItem.sum() > 0.0){
            topPSum_ += currItem.sum();
        }else{
            topMSum_ += currItem.sum();
        }
    }
}

void StatTopPMCats::splitCatSumsIntoPMCats(QMap<QString, double> topCats){
    for (QMap<QString, double>::iterator it = topCats.begin(); it != topCats.end(); ++it) {
        if ((it.value()) > 0) {
            topPCats.push_back(psd_t(it.key(), it.value()));
        } else {
            topMCats.push_back(psd_t(it.key(), it.value()));
        }
    }
}

void StatTopPMCats::sortTopPMCats(){
    std::sort(topPCats.begin(), topPCats.end(), comp_min);
    std::sort(topMCats.begin(), topMCats.end(), comp_max);
}

QString StatTopPMCats::toString(){
    QString rez = QString(">>>StatTopPMCats [report]\n");
    rez += QString("topPCats (%1):\n").arg(topPCats.size());
    for (int i = 0; i < topPCats.size(); ++i) {
        rez += QString("\t%1\t%2\n").arg(topPCats.at(i).first).arg(topPCats.at(i).second);
    }
    rez += QString("topMCats (%1):\n").arg(topMCats.size());
    for (int i = 0; i < topMCats.size(); ++i) {
        rez += QString("\t%1\t%2\n").arg(topMCats.at(i).first).arg(topMCats.at(i).second);
    }

    return rez;
}

QString StatTopPMCats::info(){
    return "StatTopPMCats [info]";
}

double StatTopPMCats::topPSum(){
    return topPSum_;
}

double StatTopPMCats::topMSum(){
    return topMSum_;
}

int StatTopPMCats::comp_min(const psd_t data1, const psd_t data2){
    return (data1.second > data2.second);
}

int StatTopPMCats::comp_max(const psd_t data1, const psd_t data2){
    return (data1.second < data2.second);
}
