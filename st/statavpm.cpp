#include "statavpm.h"

StatAvPM::StatAvPM()
{
    avP_ = 0.0;
    avM_ = 0.0;
    avP_w = 0.0;
    avM_w = 0.0;
    avP_m = 0.0;
    avM_m = 0.0;
    avP_y = 0.0;
    avM_y = 0.0;
    datesCount_ = 0;
}

void StatAvPM::generate(const Database *database)
{
    int invalidDateCount = 0;
    int validDateCount = 0;
    double sumP = 0.0;
    double sumM = 0.0;
    QSet<QDate> validDates;
    for(int i = 0; i < database->size(); i++){
        if(database->item(i).datetime().isValid()){
            validDates.insert(database->item(i).datetime());
        }else{
            invalidDateCount++;
        }
        if(database->item(i).sum() > 0){
            sumP += database->item(i).sum();
        }else{
            sumM += database->item(i).sum();
        }
    }
    validDateCount = validDates.size();
    datesCount_ = invalidDateCount + validDateCount;
    qDebug() << "datecount in database: " << datesCount_;
    avP_ = sumP / datesCount_;
    avM_ = sumM / datesCount_;
    avP_w = avP_ * 7;
    avM_w = avM_ * 7;
    avP_m = avP_ * 30;
    avM_m = avM_ * 30;
    avP_y = avP_ * 365;
    avM_y = avM_ * 365;
}

QString StatAvPM::toString()
{
    QString sReport = QString(">>>StatAvPM[report]\n\tdays:\t%1\n\tavP:\t%2\tavM:\t%3\n")
            .arg(datesCount_).arg(avP_, 2).arg(avM_, 2);
    sReport += QString("==week==\tavP:\t%1\tavM:\t%2\n==month==\tavP:\t%3\tavM:\t%4\n")
            .arg(avP_w, 2).arg(avM_w, 2).arg(avP_m, 2).arg(avM_m, 2);
    return sReport;
}

QString StatAvPM::info()
{
    return "avarage stat generator [info]";
}

double StatAvPM::avP() const
{
    return avP_;
}

double StatAvPM::avM() const
{
    return avM_;
}

double StatAvPM::avPw() const
{
    return avP_w;
}

double StatAvPM::avMw() const
{
    return avM_w;
}

double StatAvPM::avPm() const
{
    return avP_m;
}

double StatAvPM::avMm() const
{
    return avM_m;
}

double StatAvPM::avPy() const
{
    return avP_y;
}

double StatAvPM::avMy() const
{
    return avM_y;
}

