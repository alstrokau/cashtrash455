#ifndef STATTOPPMCATS_H
#define STATTOPPMCATS_H

#include "st/abstractstatmodule.h"

typedef QPair<QString, double> psd_t;
typedef QList<psd_t> lpsd_t;

class StatTopPMCats : AbstractStatModule
{
public:
    StatTopPMCats();
    
    virtual void generate(const Database *database);
    virtual QString toString();
    virtual QString info();
    lpsd_t topPCats;
    lpsd_t topMCats;
    double topPSum();
    double topMSum();
    
private:
    double topPSum_;
    double topMSum_;
    
	static int comp_max(const psd_t data1, const psd_t data2);
	static int comp_min(const psd_t data1, const psd_t data2);
    
    void clearTopCats();
    void splitCatSumsIntoPMCats(QMap<QString, double> topCats);
    void sortTopPMCats();
    void generateCatSums(const Database *database, QMap<QString, double> &topCats);
};

#endif // STATTOPPMCATS_H

