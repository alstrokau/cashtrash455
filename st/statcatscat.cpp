#include "statcatscat.h"

void StatCatScat::clearData()
{
    scatList_.clear();
    catSum_ = 0;    
}

int StatCatScat::comparator(QPair<QString, double> a, QPair<QString, double> b){
    return (a.second > b.second);
}

StatCatScat::StatCatScat(){
    clearData();
}

void StatCatScat::generate(const Database *database){
    generate(database, "");
}

void StatCatScat::transformMapToList(QMap<QString, double> buffScatMap)
{
    for (int i = 0; i < buffScatMap.size(); ++i) {
        scatList_.append(
            QPair<QString, double>(
                buffScatMap.keys().at(i), 
                buffScatMap.values().at(i)));
    }    
}

QMap<QString, double> StatCatScat::generateIntermediateMap(const QString category, const Database *database)
{
    QMap<QString, double> buffScatMap;
    for (int i = 0; i < database->size(); ++i) {
        Transaction item = database->item(i);        
        if(item.cat() == category){
            buffScatMap[item.scat()] += item.sum();
            catSum_ += item.sum();
        }
    }
    
    return buffScatMap;
}

void StatCatScat::generate(const Database *database, const QString category){
    clearData();
    
    QMap<QString, double> buffScatMap = generateIntermediateMap(category, database);        
    transformMapToList(buffScatMap);

    std::sort(scatList_.begin(), scatList_.end(), comparator);
}

QString StatCatScat::toString(){
    QString rez = QString("[StatCatScat] text report (:%1)\n").arg(scatList_.size());
    for (int i = 0; i < scatList_.size(); ++i) {
        double val = scatList_.at(i).second;
        rez += QString("\t%1\t%2(%3%)\n").arg(scatList_.at(i).first)
                .arg(val)
                .arg(val/catSum() * 100.0, 2);
    }
    
    return rez;
}

QString StatCatScat::info(){
    return QString("[StatCatScat]");
}

Common::scatList_t StatCatScat::scatList(){
    return scatList_;
}

double StatCatScat::catSum(){
    return catSum_;
}
