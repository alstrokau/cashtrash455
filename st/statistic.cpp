#include "statistic.h"

Statistic::Statistic()
{
}

void Statistic::analyzeDatabase(const Database *database)
{
    database_ = database;
    analyze();
}

void Statistic::analyze()
{
    report.generate(database_);
}
